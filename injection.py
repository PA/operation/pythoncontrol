# -*- coding: utf-8 -*-
"""
Created on Fri May 26 13:54:49 2023

@author: gamelina
"""

import numpy as np
import time
from tango import DeviceProxy, DevState

def _get_linac_table(bucket_list, fine_delay=0):
    """
    Get linac injection table to inject into a given bucket list in SPM mode.

    Parameters
    ----------
    bucket_list : array like of int or int
        Bucket number list, buckets are numbered from 1 to 416.
    fine_delay : float, optional
        Fine delay in [s]. The default is 0.

    Returns
    -------
    linac_table : array of int
        Linac injection table corresponding to the bucket_list.

    """
    if (bucket_list > 416) or (bucket_list < 1):
        raise ValueError("Wrong bucket number.")
    linac_table_zero_delay = [0, 32 , 63, 95, 126, 158, 189, 221]
    linac_step_length = 90e-12
    n_linac_step = np.round(fine_delay/linac_step_length)
    linac_table = linac_table_zero_delay[(bucket_list-1)%8] + n_linac_step
    return int(linac_table)

get_linac_table = np.vectorize(_get_linac_table)

def _get_extraction_table(bucket_list):
    """
    Get booster extraction table to inject into a given bucket list.

    Parameters
    ----------
    bucket_list : array like of int or int
        Bucket number list, buckets are numbered from 1 to 416.

    Returns
    -------
    extraction_table : array of int
        Booster extraction table corresponding to bucket_list.

    """
    if (bucket_list > 416) or (bucket_list < 1):
        raise ValueError("Wrong bucket number.")
    extraction_table = [0, 43, 34, 25, 16, 7, 50, 41, 32, 23, 14, 5, 48, 39, 
                        30, 21, 12, 3, 46, 37, 28, 19, 10, 1, 44, 35, 26, 17,
                        8, 51, 42, 33, 24, 15, 6, 49, 40, 31, 22, 13, 4, 47, 
                        38, 29, 20, 11, 2, 45, 36, 27, 18, 9]
    idx = (bucket_list - 1)//8
    return extraction_table[idx]

get_extraction_table = np.vectorize(_get_extraction_table)

def switch_injection_mode(injection_mode):
    """
    Allow to switch the injection mode.

    Parameters
    ----------
    injection_mode : "SPM" or "LPM"
        Injection "Short Pulse Mode" or "Long Pulse Mode".

    """
    spm = DeviceProxy("LIN/SY/LOCAL.SPM.1")
    lpm = DeviceProxy("LIN/SY/LOCAL.LPM.1")
    if injection_mode == "SPM":
        if spm.spmLinacEvent != 2:
            lpm.lpmEvent = 0
            spm.spmLinacEvent = 0
            time.sleep(0.1)
            spm.spmLinacEvent = 2
    elif injection_mode == "LPM":
        if lpm.lpmEvent != 2:
            spm.spmLinacEvent = 0
            lpm.lpmEvent = 0
            time.sleep(0.1)
            lpm.lpmEvent = 2
    else:
        raise ValueError("injection_mode should be 'LPM' or 'SPM'.")

def set_injection(injection_mode, bucket_list, fine_delay):
    """
    Set injection mode and bucket list to be filled.

    Parameters
    ----------
    injection_mode : "SPM" or "LPM"
        Injection "Short Pulse Mode" or "Long Pulse Mode".
    bucket_list : array like of int or int
        Bucket number list, buckets are numbered from 1 to 416.
    fine_delay : float
        Fine delay in [s].

    """
    switch_injection_mode(injection_mode)
    central = DeviceProxy("ANS/SY/CENTRAL")
    extraction = get_extraction_table(bucket_list)
    linac = get_linac_table(bucket_list, fine_delay)
    n_bucket = len(extraction)
    central.settables(np.concatenate((np.array([n_bucket]), extraction, linac)))
        
def inject_beam(booster_cav):
    """
    Inject beam into the storage ring using the current injection setup.

    Parameters
    ----------
    booster_cav : int
        Booster cavity number to be used.

    """
    if booster_cav == 1:
        cav_booster = DeviceProxy('BOO/RF/LLE.1')
        rampe = DeviceProxy("BOO/RF/RAMPETENSION")
    elif booster_cav == 2:
        cav_booster = DeviceProxy('BOO/RF/LLE.2')
        rampe = DeviceProxy("BOO/RF/RAMPETENSION.2")
    else:
        raise ValueError("booster_cav must be 1 or 2.")
        
    boo_ps = DeviceProxy("boo/ae/pstopupmodemanager")
    central = DeviceProxy("ANS/SY/CENTRAL")
    counter = 0
    
    if cav_booster.state() is DevState.ON:
        # The sleep values may need to be increased, in my shift injection with this values was not working
        if rampe.state() is not DevState.RUNNING:
            rampe.start() # Booster RF ramping
            time.sleep(1)
            boo_ps.nominal() # Booster power supply ramping
            time.sleep(5)
        while(central.state() != DevState.ON):
            counter += 1
            time.sleep(0.1)
            if counter == 100:
                raise ValueError("Central state is not ON for more than 10s.")
        central.fireburstevent()
    else:
        raise ValueError("Booster cavity state is not ON.")
        
def inject_uniform_beam(goal_current, booster_cav, fine_delay):
    """
    Inject "uniform" filling pattern by rotating the LPM injection bucket by 8
    bucket at each injection.

    Parameters
    ----------
    goal_current : float
        Total beam current to reach in [mA].
    booster_cav : int
        Booster cavity number to be used.
    fine_delay : float
        Fine delay in [s].

    """
    dcct = DeviceProxy("ANS/DG/DCCT-CTRL")
    rendement = DeviceProxy("ANS/DG/RENDEMENT")
    eff_booster = np.ones((10,))*100
    eff_sr = np.ones((10,))*100
    count = 0
    while dcct.current < goal_current:
        bucket_list = (1 + count*8)%416
        set_injection("LPM", bucket_list, fine_delay)
        inject_beam(booster_cav)
        
        time.sleep(0.5)
        
        eff_sr[0] = rendement.efficiencyANS_LPM_avg
        eff_booster[0] = rendement.efficiencyBOO_LPM_avg
        if np.mean(eff_booster) < 40:
            raise ValueError("Injection efficiency in booster is too low.")
        if np.mean(eff_sr) < 40:
            raise ValueError("Injection efficiency in storage ring is too low.")
        eff_booster = np.roll(eff_booster, 1)
        eff_sr = np.roll(eff_sr, 1)
        count += 1
        
def SPM_feedback(goal_current, booster_cav, fine_delay, max_fill=10):
    """
    SPM injection feedback to try to correct a more uniform filling pattern. 

    Parameters
    ----------
    goal_current : float
        Total beam current to reach in [mA].
    booster_cav : int
        Booster cavity number to be used.
    fine_delay : float
        Fine delay in [s].
    max_fill : int, optional
        Maximum number of bucket to fill in a single FB iteration. 
        The default is 10.

    """
    dcct = DeviceProxy("ANS/DG/DCCT-CTRL")
    rendement = DeviceProxy("ANS/DG/RENDEMENT")
    remplissage = DeviceProxy("ANS/DG/REMPLISSAGE")
    eff_booster = np.ones((10,))*100
    eff_sr = np.ones((10,))*100
    fill = remplissage.bunchesCurrent
    h = 416
    goal_per_bunch = goal_current/h
    
    while True:
        if dcct.current < goal_current:
            fill = remplissage.bunchesCurrent
            idx = np.argsort(fill)
            idx = idx[:max_fill]
            to_fill = fill[idx] < goal_per_bunch
            bucket_list = idx[to_fill] + 1
            
            print(f"Injecting in buckets: {bucket_list}")
            print(f"With current per bunch: {fill[idx[to_fill]]}")
            set_injection("SPM", bucket_list, fine_delay)
            inject_beam(booster_cav)
            
            time.sleep(0.5)
            
            eff_sr[0] = rendement.efficiencyANS_LPM_avg
            eff_booster[0] = rendement.efficiencyBOO_LPM_avg
            if np.mean(eff_booster) < 40:
                raise ValueError("Injection efficiency in booster is too low.")
            if np.mean(eff_booster) < 40:
                raise ValueError("Injection efficiency in storage ring is too low.")
            eff_booster = np.roll(eff_booster, 1)
            eff_sr = np.roll(eff_sr, 1)
        else:
            time.sleep(5)
        
    
    
    