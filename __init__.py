# -*- coding: utf-8 -*-
"""
Created on Thu Jun  1 12:21:40 2023

@author: gamelina
"""

from injection import (get_linac_table,
                       get_extraction_table,
                       switch_injection_mode,
                       set_injection,
                       inject_beam,
                       inject_uniform_beam,
                       SPM_feedback)