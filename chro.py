#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  9 15:23:24 2024

@author: gamelina
"""

import numpy as np
import matplotlib.pyplot as plt
from time import sleep
from tango import DeviceProxy
from scipy.optimize import curve_fit

#%% Init

tuneX = DeviceProxy("ans/rf/bbfdataviewer.1-tunex")
tuneZ = DeviceProxy("ans/rf/bbfdataviewer.2-tunez")
RF = DeviceProxy('ANS/RF/MasterClock')
service_locker = DeviceProxy("ANS/CA/SERVICE-LOCKER-MATLAB")
fofb_watcher = DeviceProxy("ANS/DG/FOFB-WATCHER")

#%% chroma measurement

def chroma_measurement(N_step_delta=5,
                       delta_change_max=0.002,
                       N_meas_tune=1,
                       sleep_time_tune_meas=4):
    """
    Record data for chromaticity measurement.

    Parameters
    ----------
    N_step_delta : int, optional
        Number of steps to use in chroma measurements.
        Must be an odd number.
        The default is 5.
    delta_change_max : float, optional
        Maximum relative energy (delta) variation for the chroma measurement.
        Maximum allowed is 0.004 = 0.4 % which is default value in MML.
        The default is 0.002.
    N_meas_tune : TYPE, optional
        Number of chroma measurement to do.
        The default is 1.
    sleep_time_tune_meas : float, optional
        Time to wait before the tune measurement is done.
        The default is 4.

    Returns
    -------
    delta : array of float
        Relative energy (delta) variation steps done.
    NuX : array of float
        Horizontal tune measured.
    NuZ : array of float
        Vertical tune measured.

    """
    # Parameters
    alphac = 4.1819e-04
    sleep_time_RF_change = 5 # second
    
    # Check before measurements
    if N_step_delta % 2 != 1:
        raise ValueError("N_step_delta must be an odd number.")
    
    if delta_change_max > 0.004:
        raise ValueError("Maximum energy variation should be lower or equal to 0.4 % = 0.004.")
        
    if service_locker.sofb:
        raise ValueError("Stop the SOFB")
        
    if fofb_watcher.fofbrunning_x or fofb_watcher.fofbrunning_y:
        raise ValueError("Stop the FOFB")
        
    if service_locker.isTunexFBRunning or service_locker.isTunezFBRunning:
        raise ValueError("Stop the tune FB")
    
    print("starting chroma measurement")
        
    delta = np.linspace(-delta_change_max, delta_change_max, N_step_delta)
    fRF_0 = RF.frequency
    print("Initial fRF = ", RF.frequency)
    delta_fRF_list = delta * alphac * fRF_0
    
    NuX = np.zeros((N_step_delta, N_meas_tune))
    NuZ = np.zeros((N_step_delta, N_meas_tune))
    
    for i, delta_fRF in enumerate(delta_fRF_list):
            
        RF.frequency = fRF_0 + delta_fRF
        sleep(sleep_time_RF_change)
        print("fRF = ", RF.frequency)
        while round(RF.frequency,5) != round(fRF_0 + delta_fRF,5):
            sleep(sleep_time_RF_change)
            print("fRF = ", RF.frequency)
        
        for j in range(N_meas_tune):
            sleep(sleep_time_tune_meas)
            NuX[i,j] = tuneX.Nu
            NuZ[i,j] = tuneZ.Nu
            
    RF.frequency = fRF_0
    print("chroma measurement done")
    print("Final fRF = ", RF.frequency)
            
    return (delta, NuX, NuZ)
        

#%% chroma analysis

def chro_analysis(delta, 
                  NuX, 
                  NuZ, 
                  method="lin", 
                  plot=True):
    """
    Compute chromaticity from measurement data. 

    Parameters
    ----------
    delta : array of float
        Relative energy (delta) variation steps done.
    NuX : array of float
        Horizontal tune measured.
    NuZ : array of float
        Vertical tune measured.
    method : {"lin" or "quad"}, optional
        "lin" uses a linear fit and "quad" a 2nd order polynomial. 
        The default is "lin".
    plot : bool, optional
        If True, plot the fit. 
        The default is True.

    Returns
    -------
    chro : array
        Array with horizontal and veritical chromaticity.

    """
    delta = -delta
    chro = []
    N_step_delta = len(delta)
    
    for i, Nu in enumerate([NuX, NuZ]):

        tune0 = np.mean(Nu[N_step_delta//2,:])
        dtune = np.mean(Nu[:,:],1) - tune0
        # dtune_min = np.min(Nu[:,:],1) - tune0
        # dtune_max = np.max(Nu[:,:],1) - tune0
        
        if method=="lin":
            def linear_fit(x, a, b):
                return a*x + b
            popt_lin, _ = curve_fit(linear_fit, delta, dtune)
            chro.append(popt_lin[0])
        elif method=="quad":
            def quad_fit(x, a, b, c):
                return a*x**2 + b*x + c
            popt_quad, _ = curve_fit(quad_fit, delta, dtune)
            chro.append(popt_quad[1])
            
        if plot:
            plt.figure()
            # plt.errorbar(x=delta, y=dtune, yerr=[dtune_min, dtune_max])
            plt.scatter(delta, dtune)
            if method=="lin":
                plt.plot(delta, linear_fit(delta, popt_lin[0], popt_lin[1]), '--', 
                         label="fit: {0}x+{1}".format(round(popt_lin[0],4), round(popt_lin[1],8)))
            elif method=="quad":
                plt.plot(delta, quad_fit(delta, popt_quad[0], popt_quad[1], popt_quad[2]), '--', 
                         label="fit: {0}x2+{1}x+{2}".format(round(popt_quad[0],4), round(popt_quad[1],4), round(popt_quad[2],4)))
            plt.xlabel('$\\delta$')
            if i == 0:
                plt.title("Horizontal Chromaticity")
                plt.ylabel('$\\delta nuX$')
            else:
                plt.title("Vertical Chromaticity")
                plt.ylabel('$\\delta nuZ$')
            plt.legend()
    
    print(f"chro is {chro}")
    
    return np.array(chro)
