#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 17 14:28:52 2023

@author: operateur
"""

from tango import DeviceProxy
import matplotlib.pyplot as plt
import numpy as np
import h5py as hp
from fitting import fit_risetime
from scipy.signal import hilbert
from time import gmtime, strftime
from time import sleep
from chro import chroma_measurement, chro_analysis

def take_data_gd(window_width,
                window_delay=10,
                datasize=4,
                file_name=None,
                N_record=1,
                chro_measurement=False):
    """
    Growth damp measurement with some additonal data saved.

    Parameters
    ----------
    window_width : float
        Delay generator window width in [us].
        Window during which the FBT is off.
    window_delay : float, optional
        Delay generator window delay in [ns].
        Delay of the window during which the FBT is off.
        The default is 10.
    datasize : int, optional
        Data size of the BBFV3 device.
        Increasing this value allows to save more turns.
        Be careful when increasing this value, ~10 is a good value.
        The default is 4.
    file_name : str, optional
        File name where the data is saved.
        If None, is 'growth_damp_{time}.hdf5'
        The default is None.
    N_record : int, optional
        Number of growth damp measurements to save. 
        The default is 1.
    chro_measurement : bool, optional
        If True, make de chromaticity measurement before the growth damp one. 
        The default is False.

    Returns
    -------
    file_name : str
        File name where the data is saved.

    """
    
    if chro_measurement:
        (delta, NuX, NuZ) = chroma_measurement()
        chro_lin = chro_analysis(delta, NuX, NuZ, method="lin", plot=False)
        chro_quad = chro_analysis(delta, NuX, NuZ, method="quad", plot=False)
        print(f"Chro lin: {chro_lin}")
        print(f"Chro quad: {chro_quad}")
    
    remplissage = DeviceProxy("ANS/DG/REMPLISSAGE")
    tuneX = DeviceProxy("ans/rf/bbfdataviewer.1-tunex")
    tuneZ = DeviceProxy("ans/rf/bbfdataviewer.2-tunez")
    phc_C02 = DeviceProxy("ans-c02/dg/phc-emit")
    phc_C16 = DeviceProxy("ans-c16/dg/phc-emit")
    total_current = np.round(np.sum(remplissage.bunchescurrent),1)
    
    # File setup
    if file_name is None:
        time = strftime("%H_%M_%S",gmtime())
        file_name = "growth_damp_" + str(total_current) + "mA_" + time + ".hdf5"
    file = hp.File(file_name, "a")
    
    file["filling_pattern_before"] = remplissage.bunchescurrent
    file["tuneX_before"] = tuneX.Nu
    file["tuneZ_before"] = tuneZ.Nu
    file["emitX_C02_before"] = phc_C02.EmittanceH
    file["emitX_C16_before"] = phc_C16.EmittanceH
    file["emitZ_C02_before"] = phc_C02.EmittanceV
    file["emitZ_C16_before"] = phc_C16.EmittanceV
    if chro_measurement:
        file["chro_delta"] = delta
        file["chro_NuX"] = NuX
        file["chro_NuZ"] = NuZ
        file["chro_lin"] = chro_lin
        file["chro_quad"] = chro_quad
    file.close()
    
    growth_damp(window_width=window_width,
                window_delay=window_delay,
                datasize=datasize,
                file_name=file_name,
                N_record=N_record)
    
    file = hp.File(file_name, "a")
    file["filling_pattern_after"] = remplissage.bunchescurrent
    file["tuneX_after"] = tuneX.Nu
    file["tuneZ_after"] = tuneZ.Nu
    file["emitX_C02_after"] = phc_C02.EmittanceH
    file["emitX_C16_after"] = phc_C16.EmittanceH
    file["emitZ_C02_after"] = phc_C02.EmittanceV
    file["emitZ_C16_after"] = phc_C16.EmittanceV
    file.close()
    
    return file_name

def growth_damp(window_width,
                window_delay=10,
                datasize=4,
                file_name=None,
                N_record=1):
    """
    Growth damp core function.

    Parameters
    ----------
    window_width : float
        Delay generator window width in [us].
        Window during which the FBT is off.
    window_delay : float, optional
        Delay generator window delay in [ns].
        Delay of the window during which the FBT is off.
        The default is 10.
    datasize : int, optional
        Data size of the BBFV3 device.
        Increasing this value allows to save more turns.
        Be careful when increasing this value, ~10 is a good value.
        The default is 4.
    file_name : str, optional
        File name where the data is saved.
        If None, is 'growth_damp_{time}.hdf5'
        The default is None.
    N_record : int, optional
        Number of growth damp measurements to save. 
        The default is 1.

    Returns
    -------
    file_name : str
        File name where the data is saved.

    """
    # Measurement params
    sleep_time_acquisition_on_off = 5 # second
    sleep_time_delay_generator_change = 1 # second
    h=416 # harmonic
    
    # Needed devices
    delay = DeviceProxy("ANS-C09/RF/FINE_DELAY_GENERATOR")
    service_locker = DeviceProxy("ANS/CA/SERVICE-LOCKER-MATLAB")
    fofb_watcher = DeviceProxy("ANS/DG/FOFB-WATCHER")
    tuneX = DeviceProxy("ans/rf/bbfdataviewer.1-tunex")
    tuneZ = DeviceProxy("ans/rf/bbfdataviewer.2-tunez")
    switch_FBT = DeviceProxy("ans/rf/switch_fbt")
    if switch_FBT.fbt_number1 is True:
        fbt_name = "ANS/RF/BBFV3-1"
    elif switch_FBT.fbt_number2 is True:
        fbt_name = "ANS/RF/BBFV3-2"
    else:
        ValueError("No FBT ?")
    fbt = DeviceProxy(fbt_name)
    
    if service_locker.sofb:
        raise ValueError("Stop the SOFB")
        
    if fofb_watcher.fofbrunning_x or fofb_watcher.fofbrunning_y:
        raise ValueError("Stop the FOFB")
        
    if service_locker.isTunexFBRunning or service_locker.isTunezFBRunning:
        raise ValueError("Stop the tune FB")
    
    if datasize != 4:
        # Change data size
        fbt.automaticacquisitionoff()
        sleep(sleep_time_acquisition_on_off)
        fbt.datasize = datasize
        fbt.automaticacquisitionon()
        sleep(sleep_time_acquisition_on_off)
    
    # Activate delay channel A
    delay.activateChannelA = True
    
    # Stop MNO
    selMNOsource_initial = fbt.selMNOsource
    fbt.selMNOsource = 0 # no MNO
        
    # File setup
    if file_name is None:
        time = strftime("%H_%M_%S",gmtime())
        file_name = "growth_damp_" + time + ".hdf5"
    file = hp.File(file_name, "a")
    file["window_delay"] = window_delay
    file["window_width"] = window_width
    file["datasize"] = datasize
    file["N_record"] = N_record
    
    N_data = len(np.float64(fbt.selectedBunchDataADC0))
    for j in range(N_record):
        file.create_dataset(f"raw_{j}", (h, N_data), dtype="float")
    
    # Measurment
    delay.pulseDelayChannelA = window_delay
    delay.pulseWidthChannelA = window_width
    sleep(sleep_time_delay_generator_change)
    for j in range(N_record):
        if j > 0:
            fbt.AutomaticAcquisitionON()
            sleep(sleep_time_acquisition_on_off)
        
        fbt.automaticacquisitionoff()
        sleep(sleep_time_acquisition_on_off)
        for i in range(h):
            fbt.selectedBunchNumberADC0 = i
            data = np.float64(fbt.selectedBunchDataADC0)
            file[f"raw_{j}"][i,:] = data
        
    # Get back to nominal
    delay.activateChannelA = False
    fbt.selMNOsource = selMNOsource_initial
    if datasize != 4:
        fbt.datasize = 4 # default value for operation
        fbt.AutomaticAcquisitionON()
        sleep(sleep_time_acquisition_on_off)
        # Tune devices do not work for datasize != 4, so init is needed
        tuneZ.init()
        tuneX.init()
    file.close()
    
    return file_name

def _avg_mean(signal, window):
    N=len(signal)
    data = np.zeros((N-window,))
    for i in range(N-window):
        data[i] = np.mean(signal[i:window+i])
    return data

def growth_damp_analysis(file_name,
                         dataset_name="rise_time",
                         avg_window=30,
                         min_level_select=50,
                         smoothing_window_size=10,
                         min_level_factor=1.8,
                         min_points=3,
                         min_n_risetimes=0.5,
                         save_steps=False,
                         bunch_list=None):

    file = hp.File(file_name, "a")
    h=416
    if save_steps:
        N_data = len(file["raw"][0,:])
        file.create_dataset("mean_removed", (h, N_data), dtype="float")
        file.create_dataset("hilbert", (h, N_data), dtype="float")
        file.create_dataset("data_win", (h, N_data-avg_window), dtype="float")
    file.create_dataset(dataset_name, (h,), dtype="float")
    
    if bunch_list is None:
        bunch_list = range(h)
    
    for i in bunch_list:
        data = np.array(file["raw"][i,:])
        mean_removed = data - np.mean(data)
        hilbert_transfo = np.abs(hilbert(mean_removed))
        data_win = _avg_mean(hilbert_transfo, avg_window)
        if save_steps:
            file["mean_removed"][i,:] = mean_removed
            file["hilbert"][i,:] = hilbert_transfo
            file["data_win"][i,:] = data_win
            
        min_level = np.min([np.mean(data_win[:min_level_select]),np.mean(data_win[-min_level_select:])])
        exp = fit_risetime(signal=data_win, 
                           smoothing_window_size=smoothing_window_size, 
                           min_level=min_level*min_level_factor,
                           until=None, 
                           start_from_0=False, 
                           min_points=min_points, 
                           min_n_risetimes=min_n_risetimes, 
                           matplotlib_axis=None)
        file[dataset_name][i] = exp
        
    file.close()

def plot_raw(file_name,
             bunch_list=np.arange(10)):
    
    file = hp.File(file_name, "a")
    
    plt.figure()
    plt.grid()
    for i in bunch_list:
        plt.plot(file["raw"][i,:],label="bunch = "+str(i))
    plt.legend()
    
    file.close()
    
def plot_risetime(file_name,
                  dataset_name="rise_time",
                  max_rise=5e4):

    file = hp.File(file_name, "a")
    
    h=416
    rise_time = np.array(file[dataset_name])
    file.close()
    
    idx = rise_time < 0
    rise_time[idx] = np.nan
    idx = rise_time > max_rise
    rise_time[idx] = np.nan
    
    plt.figure()
    plt.grid()
    plt.plot(np.arange(h),rise_time)
    plt.xlabel("Bunch index")
    plt.ylabel("Rise time [turns]")
    plt.title(f"mean={np.nanmean(rise_time)}, std={np.nanstd(rise_time)}")
